FROM python:3.8-slim

WORKDIR /todo

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip
RUN pip install psycopg2-binary

COPY ./requirements.txt todo/requirements.txt
RUN pip install -r todo/requirements.txt

COPY . /todo/
