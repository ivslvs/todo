from pydantic import BaseModel


class ToDoItem(BaseModel):
    id: int
    title: str
    description: str

    class Config:
        orm_mode = True


class ToDoItemIn(BaseModel):
    title: str
    description: str

    class Config:
        orm_mode = True
