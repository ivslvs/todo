import sqlalchemy
from dotenv import load_dotenv
import os


load_dotenv(".env")

metadata = sqlalchemy.MetaData()

engine = sqlalchemy.create_engine(os.environ['DATABASE_URL'])
