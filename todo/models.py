from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

from .database import engine


Base = declarative_base()


class Item(Base):
    __tablename__ = "items"
    id = Column("id", Integer, primary_key=True)
    title = Column("title", String)
    description = Column("description", String)


Base.metadata.create_all(engine)
