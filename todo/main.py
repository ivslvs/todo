from typing import List
from fastapi import FastAPI, status
from dotenv import load_dotenv
from fastapi_sqlalchemy import DBSessionMiddleware, db
from fastapi.responses import JSONResponse
import os

from .models import Item
from .schemas import ToDoItem as SchemaToDoItem, ToDoItemIn as SchemaToDoItemIn


load_dotenv(".env")

app = FastAPI()

app.add_middleware(DBSessionMiddleware, db_url=os.environ['DATABASE_URL'])


@app.get("/items/", response_model=List[SchemaToDoItem], status_code=status.HTTP_200_OK)
def get_items():
    items = db.session.query(Item).all()
    return items


@app.get("/items/{item_id}/", response_model=SchemaToDoItem, status_code=status.HTTP_200_OK)
def get_item(item_id: int):
    db_item = db.session.query(Item).filter_by(id=item_id).first()
    return db_item


@app.post("/items/", response_model=SchemaToDoItem, status_code=status.HTTP_201_CREATED)
def create_item(item: SchemaToDoItemIn):
    db_item = Item(title=item.title, description=item.description)
    db.session.add(db_item)
    db.session.commit()
    return db_item


@app.put("/items/{item_id}/", response_model=SchemaToDoItem, status_code=status.HTTP_200_OK)
def update_item(item_id: int, payload: SchemaToDoItemIn):
    db.session.query(Item).filter_by(id=item_id).update({'title': payload.title, 'description': payload.description})
    db.session.commit()

    updated_db_item = db.session.query(Item).filter_by(id=item_id).first()
    return updated_db_item


@app.delete("/items/{item_id}/", status_code=status.HTTP_200_OK)
def delete_item(item_id: int):
    db_item = db.session.query(Item).filter_by(id=item_id).delete()
    db.session.commit()
    if db_item:
        return JSONResponse({"message": "TODO Item with id: {} deleted successfully!".format(item_id)})
    return JSONResponse({"message": "It seems TODO item with id {} doesn't exist".format(item_id)})
