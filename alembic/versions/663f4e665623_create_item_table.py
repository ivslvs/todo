"""create item table

Revision ID: 663f4e665623
Revises: 
Create Date: 2021-07-02 15:22:40.990475

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '663f4e665623'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "items",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("title", sa.String),
        sa.Column("description", sa.String),
    )


def downgrade():
    op.drop_table('items')
